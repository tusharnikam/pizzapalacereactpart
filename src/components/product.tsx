import React, { useState } from 'react';
import { Button, Card, Dropdown } from 'react-bootstrap';
import { Pizza } from '../pages/Pizza';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



interface ProductProps {
  pizza: Pizza;
  moveToCart: (pizza: Pizza, size: string,quantity:number,price:number) => void;
}

const Product: React.FC<ProductProps> = ({ pizza, moveToCart }) => {
  const [selectedSize, setSelectedSize] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [selectedPrice, setSelectedPrice] = useState(0);

  const handlemoveToCart = () => {
    if(selectedSize){    moveToCart(pizza, selectedSize,quantity,selectedPrice);
    setSelectedSize('');
    setSelectedPrice(0);
    toast.success('Pizza added to cart!', { position: toast.POSITION.BOTTOM_RIGHT });
  } else {
    alert('Please select a size before adding to cart.');
  }

  };

 

  const handleSizeSelection = (event: React.ChangeEvent<HTMLInputElement>) => {
    const size = event.target.value;
     setSelectedSize(size);
    

    switch (size) {
      case 'Regular':
        setSelectedPrice(pizza.priceRegularSize);
        break;
      case 'Medium':
        setSelectedPrice(pizza.priceMediumSize);
        break;
      case 'Large':
        setSelectedPrice(pizza.priceLargeSize);
        break;
      default:
        setSelectedPrice(0);
        break;
    }
  };


  


  return (
    <Card style={{ width: '15rem', margin: '10px' }} className="hover-card">
      <Card.Img variant="top" src={pizza.imageUrl} />
      <Card.Body>
        <Card.Title>{pizza.name}</Card.Title>
        <Card.Text>{pizza.description}</Card.Text>
        <div>
      <p>Select Size:</p>
     

      <div style={{ display: 'flex', alignItems: 'center' }}>
            <label style={{ marginLeft: '25px' }}>
          <input
            type="radio"
            value="Regular"
            checked={selectedSize === "Regular"}
            onChange={handleSizeSelection}
          />
              <span style={{ marginLeft: '5px' }}>Regular - </span>
           {pizza.priceRegularSize}
        </label>
      </div>

      <div style={{ display: 'flex', alignItems: 'center' }}>
            <label style={{ marginLeft: '25px' }}>
          <input
            type="radio"
            value="Medium"
            checked={selectedSize === "Medium"}
            onChange={handleSizeSelection}
          />
      <span style={{ marginLeft: '5px' }}>Medium - </span>
       {pizza.priceMediumSize}
        </label>
      </div>

      <div style={{ display: 'flex', alignItems: 'center' }}>
            <label style={{ marginLeft: '25px' }}>
          <input
            type="radio"
            value="Large"
            checked={selectedSize === "Large"}
            onChange={handleSizeSelection}
          />
        <span style={{ marginLeft: '5px' }}>Large - </span>
         {pizza.priceLargeSize}
        </label>
      </div>
    </div>

      
        <Button variant="primary"  onClick={handlemoveToCart}>
          Add to Cart
        </Button>
      </Card.Body>
    </Card>
  );
};

export default Product;
