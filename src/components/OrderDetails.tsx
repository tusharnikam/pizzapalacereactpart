import React, {useEffect ,useState}from "react";
import axios from "axios";
import { Order } from "./Order";

import { Pizzadetails } from "./Pizzadetails";
 
const OrderDetails:React.FC= () =>{
    const [orderData,setOrderData]=useState<Order[]>([]);
    const [editingOrder, setEditingOrder] = useState<Order | null>(null);
    const [editedSize, setEditedSize] = useState('');
    const [editedQuantity, setEditedQuantity] = useState(0);
    const [editedDeliveryAddress, setEditedDeliveryAddress] = useState('');
    useEffect(() =>{

        const fetchOrderData =async ()=>{
            try{
            const response = await axios.get('http://localhost:8080/orders');
           console.log("get all orders",response.data.data);
            if(Array.isArray(response.data.data)){
                const lastOrders=response.data.data.sort((a: { orderId: number; },
                     b: { orderId: number; }) => b.orderId - a.orderId).slice(0, 1);
                setOrderData(lastOrders);
            }else{
                setOrderData([]);
            } 
        } catch (error) {
            console.error('Error fetching order data:', error);
            setOrderData([]);
        }
    };

    fetchOrderData();
  }, []);

  console.log(orderData);

  const handleEditOrder = (order: Order) => {
    setEditingOrder(order);
    setEditedSize(order.pizzas[0]?.size || '');
    setEditedQuantity(order.pizzas[0]?.quantity || 0);
    setEditedDeliveryAddress(order.deliveryAddress);
  };

  const handleUpdateOrder = async () => {
    try {
      if (!editingOrder) {
        return;
      }

      const updatedOrder: Order = {
        ...editingOrder,
        deliveryAddress: editedDeliveryAddress,
        pizzas: editingOrder.pizzas.map((pizza: Pizzadetails, index: number) => {
          if (index === 0) {
            return {
              ...pizza,
              size: editedSize,
              quantity: editedQuantity,
                subtotal: pizza.price ? pizza.price *editedQuantity : 0,
            };
          }
          return pizza;
        }),
      };
       // Send the updated order data to the API
       await axios.put(`http://localhost:8080/orders/${editingOrder.orderId}`, updatedOrder);

       // Update the local orders state with the updated order
       setOrderData((prevOrders) =>
         prevOrders.map((order) => (order.orderId === editingOrder.orderId ? updatedOrder : order))
       );
 
       // Close the edit modal or form
       setEditingOrder(null);
     } catch (error) {
       console.error('Error updating order:', error);
       // Handle the case when the API call fails
     }
   };

  return (
    <div style={{ paddingTop: "50px" }}>
    <h1>Order List</h1>
    <table style={{ width: "100%", borderCollapse: "collapse" }}>
      <thead>
        <tr>
        <th>Order Id</th>
          <th>Delivery Address</th>
          <th>Status</th>
          <th>Total Amount</th>
          <th>Pizzas</th>
          <th>Update order</th>
        </tr>
      </thead>
      <tbody>
        {orderData.map((order: Order) => (
          <tr key={order.orderId} style={{ backgroundColor: "#E6E6FA" }}>
             <td>{order.orderId}</td>
            <td>{order.deliveryAddress}</td>
            <td>{order.status}</td>
            <td>{order.totalAmount}</td>
            <td>
              {order.pizzas.map((pizza: Pizzadetails, index: number) => (
                <div key={pizza.pizzaId} style={{ marginBottom: "10px" }}>
                  <p>
                    <strong>Pizza {index + 1}:</strong>
                  </p>
                  <p>Size: {pizza.size}</p>
                  <p>Quantity: {pizza.quantity}</p>
                  <p>SubTotal: {pizza.subtotal}</p>
                </div>
              ))}
            </td>
            <td>
              {editingOrder?.orderId === order.orderId ? (
                <div>
                  <input
                    type="text"
                    value={editedDeliveryAddress}
                    onChange={(e) => setEditedDeliveryAddress(e.target.value)}
                  />
                  <br />
                  <select value={editedSize} onChange={(e) => setEditedSize(e.target.value)}>
                    <option value="Regular">Regular</option>
                    <option value="Medium">Medium</option>
                    <option value="Large">Large</option>
                  </select>
                  <br />
                  <input
                    type="number"
                    value={editedQuantity}
                    onChange={(e) => setEditedQuantity(Number(e.target.value))}
                  />
                  <button onClick={handleUpdateOrder}>OK</button>
                </div>
              ) : (
                <button onClick={() => handleEditOrder(order)}>Edit</button>
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);
};

export default OrderDetails;

