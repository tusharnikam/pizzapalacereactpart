import React from 'react';
import { Button, Card, ListGroup } from 'react-bootstrap';
import { Pizza } from '../pages/Pizza';
import  { useEffect, useState } from 'react';
import { Order } from './Order';
import axios from 'axios';

import Checkout from './OrderDetails';

import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


interface CartProps {
  cartItems: Pizza[];
  updateQuantity: (pizzaId: number, quantity: number,selectedSize:String) => void;
     
}

const Cart: React.FC<CartProps> = ({ cartItems,updateQuantity}) => {

  const [totalAmount, setTotalAmount] = useState<number>(0);
  




  useEffect(() => {
    // Update total amount when cartItems change
    setTotalAmount(cartItems.reduce((total, item) => total + item.selectedPrice * item.quantity, 0));
  }, [cartItems]);

  const createOrder= async ()=>{
    console.log(cartItems);
    try{
      const orders:Order ={
        customerId:1,
        deliveryAddress:'satara',
        totalAmount:totalAmount,
        pizzas:cartItems.map((item)=>({
          pizzaId:item.pizzaId,
          quantity:item.quantity,
          size:item.selectedSize.toString(),
          price:item.selectedPrice*item.quantity
        })),

       };
     console.log(orders);
   

     const response=await axios.post('http://localhost:8080/orders', orders);

    if(response){
      alert("you will get all orders");
     
      window.location.href='/cart';
    }
      
    }catch(error){
      console.error("Error creating order"+error);
    }
  };

  const handleCreateOrder =()=>{
    if(cartItems.length >0){
      createOrder()
     
      toast.success('Order created successfully', { position: toast.POSITION.TOP_RIGHT });
  }else{
      alert("add item in cart to create order");
    }
  };
  

  const handleQuantityChange = (pizzaId: number, quantity: number,selectedSize:String) => {
    updateQuantity(pizzaId, quantity,selectedSize); 
  };

 
   
  return (
  
    <Card style={{ margin: '10px' ,width: '100%'}}>
    <Card.Body>
      <Card.Title>Your Cart</Card.Title>
      {cartItems.length === 0 ? (
        <Card.Text>
          <svg xmlns="http://www.w3.org/2000/svg" width="150" height="150" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
          </svg>
          <h5>Oops! Your Cart is empty. Add Products</h5>
        </Card.Text>
      ) : (
        <div style={{ maxHeight: '600px',maxWidth:'600', overflowY: 'auto' }}>
          <table className="table table-bordered"style={{ width: '100%' }}>
            <thead>
              <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
              {cartItems.map((item) => (
                <tr key={item.pizzaId}>
                  <td>
                    <Card.Img variant="top" src={item.imageUrl} style={{ width: '55px' }} />
                  </td>
                  <td>{item.name}</td>
                  <td>{item.selectedSize}</td>
                  <td>
                    <Button variant="outline-primary" onClick={() => handleQuantityChange(item.pizzaId, item.quantity - 1, item.selectedSize)}>-</Button>
                    <span className="mx-2">{item.quantity}</span>
                    <Button variant="outline-primary" onClick={() => handleQuantityChange(item.pizzaId, item.quantity + 1, item.selectedSize)}>+</Button>
                  </td>
                  <td>{item.selectedPrice}</td>
                  <td>{item.selectedPrice * item.quantity}</td>
                  <Card style={{ backgroundColor: "#EEE" }}>
            
          
          </Card>
                </tr>
              ))}
            </tbody>
          </table>
          <div className="total-amount">
          <strong>Total Amount:</strong> {totalAmount}
          </div>
          <Button style={{ marginTop: '10px' }} variant="primary" onClick={handleCreateOrder}>
        Create Order
       </Button>
       

        </div>
      
        
      )}
    </Card.Body>
  </Card>
  

  );
};

export default Cart;
