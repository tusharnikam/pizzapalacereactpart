import { Pizzadetails } from "./Pizzadetails";
export interface Order{
    orderId?: number;
    status?:string;
    customerId?:number;
    totalAmount:number;
    deliveryAddress:string;
    orderDateTime?:string;
    pizzas:Pizzadetails[];
    
}