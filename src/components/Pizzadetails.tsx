export interface Pizzadetails{
    pizzaId:number;
    quantity:number;
    size:string;
    price:number;
    subtotal?:number; 
}