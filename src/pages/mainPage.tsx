import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import Product from '../components/product';
import Cart from '../components/cart';
import axios from 'axios';
import { Pizza } from './Pizza';

const MainPage = () => {
  const [pizzas, setPizzas] = useState<Pizza[]>([]);
  const [cartItems, setCartItems] = useState<Pizza[]>([]);
  


  useEffect(() => {
    fetchAllPizzas();
  }, []);

  const fetchAllPizzas = async () => {
    try {
      const response = await axios.get('http://localhost:8080/pizzas');
      setPizzas(response.data.Data);
    } catch (error) {
      console.error('Error fetching pizzas:', error);
    }
  };

  const moveToCart = (pizza: Pizza, selectedSize: String, quantity: number, selectedPrice: number) => {

    const existedPizza = cartItems.find((item) => item.pizzaId === pizza.pizzaId && item.selectedSize == selectedSize);

    if (existedPizza) {
      const updatedItems = cartItems.map((item) => {
        if (item.pizzaId === existedPizza.pizzaId && item.selectedSize === selectedSize) {
          return { ...item, quantity: item.quantity + quantity, selectedPrice, selectedSize: selectedSize || '' };
        }
        return item;
      });

      setCartItems(updatedItems);
    } else {
      const newPizza = { ...pizza, selectedSize: selectedSize || '', quantity, selectedPrice };

      setCartItems((prevItems) => [...prevItems, newPizza]);
    }

  };

  const updateQuantity = (pizzaId: number, quantity: number, selectedSize: String) => {
    if (quantity < 1) {
      removeCartItem(pizzaId, selectedSize);
    } else {
      setCartItems((prevItems) =>
        prevItems.map((item) =>
          item.pizzaId === pizzaId && item.selectedSize === selectedSize ? { ...item, quantity: quantity } : item
        )
      );
    }
    console.log(selectedSize);


  };
  

  const removeCartItem = (pizzaId: number, selectedSize: String) => {
    setCartItems((prevItems) => prevItems.filter((item) => item.pizzaId !== pizzaId || item.selectedSize !== selectedSize));

  };
  return (
    <Container style={{ marginTop: '80px', paddingTop: '50px', backgroundColor: '#E6E6FA' }}>
      <Row>
        <Col xs sm={8} lg={8}>
          <h3>Our Pizzas</h3>
        </Col>
        {/* <Col xs sm={4} lg={4}>
          <div>
            <h4>Information</h4>
          </div>
        </Col> */}
        <Col xs sm={8} lg={8}>
          <Card style={{ backgroundColor: 	'#E6E6FA' }}>
          <Row>
              {pizzas.map(pizza => (
                <Col key={pizza.pizzaId}>
                  <Product key={pizza.pizzaId} pizza={pizza} moveToCart={moveToCart}  />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: '#E6E6FA' }}>
            <Cart cartItems={cartItems} updateQuantity={updateQuantity} />
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default MainPage;
