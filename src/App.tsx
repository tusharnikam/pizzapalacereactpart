import './App.css';
import Content from './common/content';
import Footer from './common/footer';
import NavbarComponent from './common/navbar';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
function App() {
  return (
    <div className="App" style={{backgroundColor:"#EEE"}}>
        <NavbarComponent/>
        <Content/>
        <Footer/>
        <div>
      {/* Your other components and routes */}
      <ToastContainer position="top-right" autoClose={1500} hideProgressBar />
    </div>
    </div>
  );
}

export default App;
