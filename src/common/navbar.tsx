
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import './navbar.css';

const NavbarComponent = () => {
    return (
        <nav style={{ backgroundColor: 'lightblue', padding: '10px', marginBottom: '20px' }}>
         <Navbar fixed="top" className="bg-body-tertiary">
        <Container>
        <Navbar.Brand href="#home">
                <img
                    src={"https://scontent.fbom3-1.fna.fbcdn.net/v/t39.30808-6/274541822_5290934254272341_1788601755705677667_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=6NF4TLCIJPMAX8sHj8r&_nc_ht=scontent.fbom3-1.fna&oh=00_AfB1MrF6h5zedZtJiWK2fyy1FihFimHskM5qg6VxWR6KhA&oe=64B5DB12"}
                   width="80"
                    height="80"
                    className="d-inline-block align-top"
                    alt="Logo"
                />

            <div className="logo-text">Pizza Palace</div>
            </Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    Signed in as: <a href="#login"> User 1</a>
                </Navbar.Text>
            </Navbar.Collapse>
        </Container>
    </Navbar>
      </nav>
       
    )
    
}

export default NavbarComponent;