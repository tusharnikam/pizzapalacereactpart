import React from "react"

const Footer = () => (
    <footer className="page-footer font-small blue mt-4" style={{ 
      backgroundColor: `#E6E6FA`, paddingTop: "10px", paddingBottom: "10px"
    }}>
      <div className="container-fluid text-center text-md-left">
        <div className="row mt-2">
          <div className="col-md-6">
            <h5 className="text-uppercase">Tushar's Pizza Palace</h5>
            <p style={{ fontSize: "14px" }}>Best pizzas in the town</p>
          </div>
          <hr className="clearfix w-100 d-md-none pb-0" />
          <div className="col-md-3 mb-md-0 mb-3">
            <h5 className="text-uppercase">Contact us</h5>
            <ul className="list-unstyled" style={{ fontSize: "14px" }}>
              <li><a href="#!">PizzaPalace@gmail.com</a></li>
              <li><p>8080808080</p></li>
            </ul>
          </div>
          <div className="col-md-3 mb-md-0 mb-3">
            <h5 className="text-uppercase">Address</h5>
            <ul className="list-unstyled" style={{ fontSize: "14px" }}>
              <li><a href="#!">All around the globe</a></li>
              <li><a href="#!">Search the place on your own</a></li>
            </ul>
          </div>
        </div>
      </div>
  
      <div className="footer-copyright text-center py-2" style={{ fontSize: "14px" }}>
        © 2023 PizzaPalace.com
      </div>
    </footer>
  );
  
  export default Footer;
  